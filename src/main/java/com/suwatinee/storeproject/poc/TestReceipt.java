/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suwatinee.storeproject.poc;

import model.Customer;
import model.Product;
import model.Receipt;
import model.User;

/**
 *
 * @author suwat
 */
public class TestReceipt {
    public static void main(String[] args) {
        Product p1 = new Product(1,"Chayen",30);
        Product p2 = new Product(2,"Americano",40);
        User seller = new User("Naded","0900865555","password");
        Customer customer = new Customer("Pawit","0898985412");
        Receipt receipt = new Receipt(seller,customer);
        receipt.addRecepitDetail(p1, 1);
        receipt.addRecepitDetail(p2, 3);
        System.out.println(receipt);
        receipt.deleteReceiptDetail(0);
        System.out.println(receipt);
        receipt.addRecepitDetail(p1, 2);
        System.out.println(receipt);
        
    }
}
